package com.gitee.kamismile.stoneEureka.common;

import com.gitee.kamismile.gatewayAgent.server.bootstrap.GateWayServer;
import com.gitee.kamismile.stoneEureka.server.EnableServerEureka;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.security.reactive.ReactiveSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

/**
 * Created by lidong on 2017/2/13.
 */
@SpringBootApplication
@ImportResource("classpath*:config/*.xml")
@EnableServerEureka
@EnableAutoConfiguration(exclude = {
        SecurityAutoConfiguration.class,
        ReactiveSecurityAutoConfiguration.class,
        DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class})
public class Application {


    public static void main(String[] args) throws Exception {
        ConfigurableApplicationContext ac = new SpringApplicationBuilder(Application.class)
                .web(WebApplicationType.NONE).run(args);
        GateWayServer gws = ac.getBean(GateWayServer.class);
        gws.start();
    }


}
