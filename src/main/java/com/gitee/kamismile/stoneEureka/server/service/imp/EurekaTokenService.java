package com.gitee.kamismile.stoneEureka.server.service.imp;

import com.gitee.kamismile.stoneEureka.server.service.IEurekaService;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class EurekaTokenService implements IEurekaService {

    String cmd = "app@register@token";

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public Map<String, Object> doService(Map<String, Object> data) {
        Object token = data.get("token");
        if (!Objects.isNull(token)) {
            return data;
        }
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("token", UUID.randomUUID().toString());
        return map;
    }

    @Override
    public void delete(String routeId) {

    }

    @Override
    public boolean isCmd(String cmd) {
        return this.cmd.equals(cmd);
    }
}
