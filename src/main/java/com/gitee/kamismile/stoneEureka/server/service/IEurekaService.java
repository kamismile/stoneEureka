package com.gitee.kamismile.stoneEureka.server.service;

import java.util.Map;

public interface IEurekaService {

    public Map<String,Object> doService(Map<String,Object> data);

    public boolean isCmd(String cmd);

    public void delete(String routeId) ;

}
