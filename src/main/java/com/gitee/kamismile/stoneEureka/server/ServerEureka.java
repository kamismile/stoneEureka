package com.gitee.kamismile.stoneEureka.server;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = "com.gitee.kamismile.stoneEureka.server")
public class ServerEureka {
}
