package com.gitee.kamismile.stoneEureka.server.service.imp;

import com.gitee.kamismile.stone.commmon.util.JsonUtil;
import com.gitee.kamismile.stoneEureka.server.service.IEurekaService;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import com.gitee.kamismile.stoneEureka.server.route.RedisRouteDefinitionRepository;
import java.util.HashMap;
import java.util.Map;

@Component
public class EurekaServerInfoSycnService implements IEurekaService {
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    String cmd = "app@server@sync";

    @Autowired
    @Lazy
    private RedisRouteDefinitionRepository redisRouteDefinitionRepository;

    @Override
    public Map<String, Object> doService(Map<String, Object> data) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("routeDefinitions", JsonUtil.toJson(redisRouteDefinitionRepository.getRouteDefinitions()));
        return map;
    }

    @Override
    public void delete(String routeId) {

    }

    @Override
    public boolean isCmd(String cmd) {
        return this.cmd.equals(cmd);
    }
}
