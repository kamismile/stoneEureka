/**
 * LY.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package com.gitee.kamismile.stoneEureka.server.handler;

import com.gitee.kamismile.gatewayAgent.protobuf.Message;
import com.gitee.kamismile.gatewayAgent.server.bootstrap.GatewayAgentConstant;
import com.gitee.kamismile.gatewayAgent.server.handler.GateWayServerInitializer;
import com.gitee.kamismile.stone.commmon.util.SnowflakeIdWorker;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.codec.protobuf.ProtobufEncoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32FrameDecoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32LengthFieldPrepender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * @author dong.li
 * @version $Id: EurekaServerInitializer, v 0.1 2018/10/22 18:02 dong.li Exp $
 */
@Component
public class EurekaServerInitializer extends GateWayServerInitializer {

    @Autowired
    private SnowflakeIdWorker snowflakeIdWorker;
    @Autowired
    private GatewayAgentConstant gatewayAgentConstant;
    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public void initChannel(SocketChannel ch) {
        ChannelPipeline p = ch.pipeline();


        p.addLast(new ChannelHandler[]{new ProtobufVarint32FrameDecoder()});
        p.addLast(new ChannelHandler[]{new ProtobufDecoder(Message.RouterMessage.getDefaultInstance())});
        p.addLast(new ChannelHandler[]{new ProtobufVarint32LengthFieldPrepender()});
        p.addLast(new ChannelHandler[]{new ProtobufEncoder()});
        EurekaServerHandler gateWayServerHandler = new EurekaServerHandler();
        gateWayServerHandler.setGatewayAgentConstant(this.gatewayAgentConstant);
        gateWayServerHandler.setSnowflakeIdWorker(this.snowflakeIdWorker);
        gateWayServerHandler.setApplicationContext(applicationContext);
        p.addLast(new ChannelHandler[]{gateWayServerHandler});
    }

}
