package com.gitee.kamismile.stoneEureka.server;


import com.gitee.kamismile.gatewayAgent.server.bootstrap.EnableServerAgent;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(ServerEureka.class)
@EnableServerAgent
public @interface EnableServerEureka {
}
