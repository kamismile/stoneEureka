package com.gitee.kamismile.stoneEureka.utils;

public class NameUtils {
    public static final String GENERATED_NAME_PREFIX = "_genkey_";

    public static String generateName(int i) {
        return GENERATED_NAME_PREFIX + i;
    }
}
