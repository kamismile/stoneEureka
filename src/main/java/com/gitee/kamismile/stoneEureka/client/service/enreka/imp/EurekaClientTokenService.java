package com.gitee.kamismile.stoneEureka.client.service.enreka.imp;

import com.gitee.kamismile.stone.commmon.util.JsonUtil;
import com.gitee.kamismile.stone.commmon.util.ValueUtils;
import com.gitee.kamismile.stoneEureka.client.EurekaClientConstant;
import com.gitee.kamismile.stoneEureka.client.common.ClientChannel;
import com.gitee.kamismile.stoneEureka.client.handler.EureKaClientHandler;
import com.gitee.kamismile.stoneEureka.client.service.enreka.IEurekaClientService;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class EurekaClientTokenService implements IEurekaClientService {

    String cmd = "app@register@token";


    @Override
    public void doService(Map<String, Object> data, EureKaClientHandler eureKaClientHandler) {
        EurekaClientConstant.AppProvider app = eureKaClientHandler.getEurekaClientConstant().getApp().get("application");
        String versionCmd = "app@register@version";
        if (ValueUtils.isNull(app.getAppPath())) {
            versionCmd = "app@vertx@version";
        }

        Map<String, Object> map = JsonUtil.json2Map(JsonUtil.toJson(app));
        map.put("token", data.get("token"));
        ClientChannel.token.put("token", data.get("token").toString());
        eureKaClientHandler.sendMessage(versionCmd, JsonUtil.toJson(map), 2L, "1");
    }

    @Override
    public boolean isCmd(String cmd) {
        return this.cmd.equals(cmd);
    }
}
