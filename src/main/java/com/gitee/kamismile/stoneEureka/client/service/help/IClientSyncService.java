package com.gitee.kamismile.stoneEureka.client.service.help;

import com.gitee.kamismile.stoneEureka.pojo.RouteDefinition;

import java.util.List;

public  interface IClientSyncService {
    void doProcess(List<RouteDefinition> routeDefinitionList);
}
