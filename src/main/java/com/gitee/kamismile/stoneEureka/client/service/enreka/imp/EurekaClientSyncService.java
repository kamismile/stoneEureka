package com.gitee.kamismile.stoneEureka.client.service.enreka.imp;

import com.gitee.kamismile.stone.commmon.util.JsonUtil;
import com.gitee.kamismile.stone.commmon.util.ValueUtils;
import com.gitee.kamismile.stoneEureka.client.EurekaClientConstant;
import com.gitee.kamismile.stoneEureka.client.handler.EureKaClientHandler;
import com.gitee.kamismile.stoneEureka.pojo.RouteDefinition;
import com.google.gson.reflect.TypeToken;
import com.gitee.kamismile.stoneEureka.client.service.enreka.IEurekaClientService;
import com.gitee.kamismile.stoneEureka.client.service.help.IClientSyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class EurekaClientSyncService implements IEurekaClientService {

    String cmd = "app@server@sync";

    @Autowired
    private IClientSyncService iClientSyncService;

    @Override
    public void doService(Map<String, Object> data, EureKaClientHandler eureKaClientHandler) {
        EurekaClientConstant.AppProvider app = eureKaClientHandler.getEurekaClientConstant().getApp().get("application");

        if (!ValueUtils.isNull(app.getAppPath())) {
            return;
        }

        Object routeDefinitions = data.get("routeDefinitions");
        if(ValueUtils.isNull(routeDefinitions)){
            return;
        }

        List<RouteDefinition> routeDefinitionList = JsonUtil.fromJson(routeDefinitions.toString(), new TypeToken<List<RouteDefinition>>() {
        }.getType());

        iClientSyncService.doProcess(routeDefinitionList);
    }

    @Override
    public boolean isCmd(String cmd) {
        return this.cmd.equals(cmd);
    }
}
