package com.gitee.kamismile.stoneEureka.client;


import com.gitee.kamismile.stoneEureka.client.service.help.IClientSyncService;
import com.gitee.kamismile.stoneEureka.client.service.help.imp.ClientSyncServiceHelp;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.gitee.kamismile.stoneEureka.client")
public class ClientEureka {

    @Bean
    @ConditionalOnMissingBean(IClientSyncService.class)
    public IClientSyncService iClientSyncService(){
        return new ClientSyncServiceHelp();
    }

}
