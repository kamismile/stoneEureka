package com.gitee.kamismile.stoneEureka.client;


import com.gitee.kamismile.gatewayAgent.client.bootstrap.EnableClientAgent;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(ClientEureka.class)
@EnableClientAgent
public @interface EnableClientEureka {
}
