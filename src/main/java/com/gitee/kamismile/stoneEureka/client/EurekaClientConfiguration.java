package com.gitee.kamismile.stoneEureka.client;

import com.gitee.kamismile.gatewayAgent.client.bootstrap.GateWayClient;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;



@Configuration
public class EurekaClientConfiguration {

    String cmd = "app@register@version";
    @Autowired
    EurekaClientConstant eurekaClientConstant;
    @Autowired
    GateWayClient gateWayClient;

    @PostConstruct
    void register() throws Exception {
        gateWayClient.start();
//        EurekaClientConstant.AppProvider app = eurekaClientConstant.getApp().get("application");
//        if (ValueUtils.isNotNull(app.getAppPath())) {
//            if (ValueUtils.isNotNull(gateWayClient)) {
//                gateWayClient.sendMessage(cmd, JsonUtil.toJson(app),1L,"1");
//            }
//        }
    }
}
