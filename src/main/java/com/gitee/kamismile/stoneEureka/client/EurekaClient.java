/**
 * LY.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package com.gitee.kamismile.stoneEureka.client;

import com.gitee.kamismile.gatewayAgent.client.bootstrap.EnableClientAgent;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.security.reactive.ReactiveSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

import java.util.concurrent.TimeUnit;

/**
 * @author dong.li
 * @version $Id: EurekaClientInitializer, v 0.1 2018/10/22 18:02 dong.li Exp $
 */

@SpringBootApplication
@ImportResource("classpath*:config/*.xml")
@EnableClientEureka
@EnableAutoConfiguration(exclude = {
        SecurityAutoConfiguration.class,
        ReactiveSecurityAutoConfiguration.class,
        DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class})
public class EurekaClient {
    public static void main(String[] args) throws Exception {
        ConfigurableApplicationContext ac = new SpringApplicationBuilder(EurekaClient.class)
                .web(WebApplicationType.NONE).run(args);
        TimeUnit.MINUTES.sleep(5);
//        GateWayClient client = ac.getBean(GateWayClient.class);
//        client.start();
//        client.sendMessage("app@register@version", "{\"token\":\"bc4cdc06-8b62-446a-9d61-a30f884a333f\",\"gameID\":\"1\",\"uid\":\"319797623106572288\"}");
    }
}
