package com.gitee.kamismile.stoneEureka.client.service.enreka.imp;

import com.gitee.kamismile.stone.commmon.util.JsonUtil;
import com.gitee.kamismile.stone.commmon.util.ValueUtils;
import com.gitee.kamismile.stoneEureka.client.EurekaClientConstant;
import com.gitee.kamismile.stoneEureka.client.common.ClientChannel;
import com.gitee.kamismile.stoneEureka.client.handler.EureKaClientHandler;
import com.gitee.kamismile.stoneEureka.client.service.enreka.IEurekaClientService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class EurekaClientVersionService implements IEurekaClientService {

    String cmd = "app@vertx@sync";

    @Override
    public void doService(Map<String, Object> data, EureKaClientHandler eureKaClientHandler) {
        EurekaClientConstant.AppProvider app = eureKaClientHandler.getEurekaClientConstant().getApp().get("application");

        if (!ValueUtils.isNull(app.getAppPath())) {
            return;
        }

        Map<String, Object> map = new HashMap<>();
        String token = ClientChannel.token.get("token");
        if (StringUtils.isNoneBlank(token)) {
            map.put("token", token);
        }
        String versionCmd = "app@server@sync";
        eureKaClientHandler.sendMessage(versionCmd, JsonUtil.toJson(map), 3L, "1");
    }

    @Override
    public boolean isCmd(String cmd) {
        return this.cmd.equals(cmd);
    }
}
