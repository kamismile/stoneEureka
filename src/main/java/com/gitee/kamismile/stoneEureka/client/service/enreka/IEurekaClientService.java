package com.gitee.kamismile.stoneEureka.client.service.enreka;

import com.gitee.kamismile.stoneEureka.client.handler.EureKaClientHandler;

import java.util.Map;

public interface IEurekaClientService {

    public void doService(Map<String, Object> data, EureKaClientHandler eureKaClientHandler);

    public boolean isCmd(String cmd);
}
