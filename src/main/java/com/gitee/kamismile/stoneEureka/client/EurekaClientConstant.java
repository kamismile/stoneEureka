package com.gitee.kamismile.stoneEureka.client;

import com.gitee.kamismile.stone.commmon.exception.BusinessException;
import com.gitee.kamismile.stoneEureka.utils.NameUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.util.StringUtils.tokenizeToStringArray;

@Configuration
@ConfigurationProperties(prefix = "eurekaclient.constant")
public class EurekaClientConstant {


    private final Map<String, EurekaClientConstant.AppProvider> app = new HashMap<>();

    public Map<String, AppProvider> getApp() {
        return app;
    }

    public static class AppProvider {
        private String appName;
        private String appPath;
        private String appVersion;
        private String appServer;
        private  String appEnable;//1启用
        private List<AppPredicateDefinition> appPredicates;
        private List<AppFilterDefinition> appFilters;
        private Map<String, Object> metadata = new HashMap();

        public Map<String, Object> getMetadata() {
            return metadata;
        }

        public void setMetadata(Map<String, Object> metadata) {
            this.metadata = metadata;
        }

        public List<AppPredicateDefinition> getAppPredicates() {
            return appPredicates;
        }

        public void setAppPredicates(List<AppPredicateDefinition> appPredicates) {
            this.appPredicates = appPredicates;
        }

        public List<AppFilterDefinition> getAppFilters() {
            return appFilters;
        }

        public void setAppFilters(List<AppFilterDefinition> appFilters) {
            this.appFilters = appFilters;
        }

        public String getAppServer() {
            return appServer;
        }

        public void setAppServer(String appServer) {
            this.appServer = appServer;
        }

        public String getAppName() {
            return appName;
        }

        public void setAppName(String appName) {
            this.appName = appName;
        }

        public String getAppPath() {
            return appPath;
        }

        public String getAppEnable() {
            return appEnable;
        }

        public void setAppEnable(String appEnable) {
            this.appEnable = appEnable;
        }

        public void setAppPath(String appPath) {
            this.appPath = appPath;
        }

        public String getAppVersion() {
            return appVersion;
        }

        public void setAppVersion(String appVersion) {
            this.appVersion = appVersion;
        }
    }

    public static class AppPredicateDefinition {
        private String name;
        private Map<String, String> args = new LinkedHashMap<>();

        public AppPredicateDefinition(String text) {
            int eqIdx = text.indexOf("=");
            if (eqIdx <= 0) {
                throw new BusinessException("Unable to parse PredicateDefinition text '" + text + "'" +
                        ", must be of the form name=value");
            }
            setName(text.substring(0, eqIdx));

            String[] args = tokenizeToStringArray(text.substring(eqIdx + 1), ",");

            for (int i = 0; i < args.length; i++) {
                this.args.put(NameUtils.generateName(i), args[i]);
            }
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Map<String, String> getArgs() {
            return args;
        }

        public void setArgs(Map<String, String> args) {
            this.args = args;
        }
    }

    public static class AppFilterDefinition {
        private String name;
        private Map<String, String> args = new LinkedHashMap<>();

        public AppFilterDefinition(String text) {
            int eqIdx = text.indexOf('=');
            if (eqIdx <= 0) {
                setName(text);
                return;
            }
            setName(text.substring(0, eqIdx));

            String[] args = tokenizeToStringArray(text.substring(eqIdx + 1), ",");

            for (int i = 0; i < args.length; i++) {
                this.args.put(NameUtils.generateName(i), args[i]);
            }
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Map<String, String> getArgs() {
            return args;
        }

        public void setArgs(Map<String, String> args) {
            this.args = args;
        }
    }
}
